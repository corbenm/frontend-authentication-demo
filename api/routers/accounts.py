from fastapi import APIRouter, Depends, Request
from fastapi.middleware.cors import CORSMiddleware

from models.accounts import AccountIn, AccountOut, AccountWithHashedPassword, AccountToken
from queries.accounts import AccountRepo
from authenticator import authenticator

router = APIRouter()


@router.post("/api/accounts/")
async def create_account(
    account: AccountIn,
    account_repo: AccountRepo = Depends()
) -> AccountOut:
    hashed_password = authenticator.hash_password(account.password)
    return account_repo.create(AccountWithHashedPassword(hashed_password=hashed_password, **account.dict()))

@router.get("/token")
async def get_by_cookie(
    request: Request,
    account_data: dict | None = Depends(authenticator.try_get_current_account_data)
) -> AccountToken | None:
    if account_data and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account_data,
        }
    
@router.get("/api/accounts")
async def get_account(
    account_data: dict | None = Depends(authenticator.try_get_current_account_data)
) -> AccountOut | None:
    return account_data